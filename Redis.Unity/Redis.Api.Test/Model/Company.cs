﻿namespace Redis.Api.Test;

public class Company
{
    public string Name { get; set; }

    public string Vat { get; set; }

    public string Address { get; set; }

    public string District { get; set; }

    public string Zipcode { get; set; }

    public string City { get; set; }

    public string Phone { get; set; }

    public string Fax { get; set; }

    public string Country { get; set; }
}