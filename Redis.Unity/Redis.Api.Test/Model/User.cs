﻿using System.Globalization;

namespace Redis.Api.Test;

public class User
{
    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public CultureInfo Culture { get; set; }
    public string TimeZoneId { get; set; }
    public bool EmailConfirmed { get; set; }
    public Company Company { get; set; }
}