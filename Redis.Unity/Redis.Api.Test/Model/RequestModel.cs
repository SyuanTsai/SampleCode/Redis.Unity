﻿namespace Redis.Api.Test;

/// <summary>
///     請求模型
/// </summary>
/// <typeparam name="TEntity"></typeparam>
public class RequestModel<TEntity>
{
    /// <summary>
    ///     Redis Key
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    ///     資料內容
    /// </summary>
    public TEntity Data { get; set; }
}