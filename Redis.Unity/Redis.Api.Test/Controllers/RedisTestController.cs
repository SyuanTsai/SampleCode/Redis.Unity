using Microsoft.AspNetCore.Mvc;
using Redis.Unity.Interface;

namespace Redis.Api.Test.Controllers;

[ApiController]
[Route("[controller]")]
public class RedisTestController : ControllerBase
{
    private readonly IRedisUnity _redis;

    public RedisTestController(IRedisUnity redis)
    {
        _redis = redis;
    }
    

    [HttpPost(Name = "SetData")]
    public IEnumerable<WeatherForecast> SetData(RequestModel<User> Data)
    {
        return null;
    }

}