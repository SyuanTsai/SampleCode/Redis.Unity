﻿using StackExchange.Redis.Extensions.Core.Abstractions;

namespace Redis.Unity.Interface;

/// <summary>
///     Redis 統一存取單元
/// </summary>
public class RedisUnity : IRedisUnity
{
    private readonly IRedisClient _redisClient;

    public RedisUnity(IRedisClient redisClient)
    {
        _redisClient = redisClient;
    }
    
    
}