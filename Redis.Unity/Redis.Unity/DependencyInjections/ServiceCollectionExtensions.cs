﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.System.Text.Json;

namespace Redis.Unity.DependencyInjections;

/// <summary>
///     Redis Service 擴展
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    ///     新增Redis的基本設定
    /// </summary>
    /// <param name="services"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    public static IServiceCollection AddRedisSetting(this IServiceCollection services
                                                   , IConfiguration          config)
    {
        services.AddStackExchangeRedisExtensions<SystemTextJsonSerializer>
        (
            config.GetSection("Redis")
                  .Get<RedisConfiguration>()
        );

        return services;
    }
}